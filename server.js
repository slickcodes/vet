const express = require('express');
const app = express();
const path = __dirname + '/views/';


// MIDDLE WARE
// detect the HTTP request.
app.use(function(req, res, next){
	console.log("/" + req.method);
	next();
});


// REST API
app.get("/", function (req, res) {
  //res.send("id:" + req.query.id);
  res.sendFile(path + 'client.html');
});

app.get("/about", function(req, res){
  res.sendFile(path + "about.html");
});

app.get("/contact", function(req, res){
	res.sendFile(path + "contact.html");
})

app.use("*", function(req, res){
	res.sendFile(path + "404.html");
});



app.post('/', function (req, res) {
  res.send('Got a POST request')
});

app.put('/', function (req, res) {
  res.send('Got a PUT request at /user')
});

app.delete('/', function (req, res) {
  res.send('Got a DELETE request at /user')
});


/*
 * FEATURE LIST 
 * client records
 * calendar to manage appointments
 * reminder systems
 * animal registration
 * check up records
 * inventory management
 * invoices & payments
 * reporting
 */



/* the port number of the app */
app.listen(80, function () {
  console.log('Example app listening on port 80!')
});